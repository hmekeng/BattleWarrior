﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarriorBattle
{
    class Warrior
    {
        // here we define a propretie we are  going to have 
        // Name , Health, Attack Maximum, Block Maximum
        public string Name { get; set; } = "Warrior";  // warrior is the default value
        public double Health { get; set; } = 0;
        public double AttkMax { get; set; } = 0;
        public double BlockMax { get; set; } = 0;
        

        // Ramdom numbers we want the attack to be random
        Random rnd = new Random();

        // a constructor

        public Warrior(string name= "Warrior", double health=0, double attkMax=0, double blockMax=0)
        {
            Name = name;
            Health = health;
            AttkMax = attkMax;
            BlockMax = blockMax;

        }


        // Attack function
        // Generate a ramdom attack from 1 to the maximum attack

        public double Attack()
        {
            // here we convert double into integer 
            return rnd.Next(1, (int)AttkMax);
        }

        //Block 
        // Generate a ramdom block from 1 to the maximum block
        public double Block()
        {
            return rnd.Next(1, (int)BlockMax);
        }
    }
}
